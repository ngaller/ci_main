# Gitlab CI + Submodules

https://git-scm.com/book/en/v2/Git-Tools-Submodules

Add a submodule:

    git submodule add git@gitlab.com:ngaller/ci_res.git
	
If the module is on the same gitlab server, use a relative url instead (important for CI):

    git submodule add ../ci_res.git

Checkout submodule code:

 - when cloning, the submodule will not be cloned by default
 - to get the submodule code, you have to do (alias **gSI = init + update**):

		git submodule init
		git submodule update
		
 - it's also possible to get them during the initial clone (alias **gfcr**):

		git clone --recurse-submodules git@gitlab.com:ngaller/ci_main.git
		
 - to get update on the submodule, alias **gSu**.  This will pull the latest commit for the
   submodule

		git submodule update --remote
		
 - when pulling changes on the main project, it will not by default update the submodules, you have to `gSu`, or set option submodule.recurse 

    - at that point you can `commit` to associate a particular commit with the project (`git diff`
      will show the change)
    - but when you do `git pull` on another copy of the project, it will not fetch the submodule!
      In fact it will even show that you have a local change because your local submodule is lagging
      behind the commit pointed to in master.
      To get that, you have to use `git config submodule.recurse`, or do `git pull --recurse-submodules` - this is a better practice, if you
      don't want to accidentally update the submodule.
		
 - you can have the submodule track a particular branch, just add "branch = ..." in .gitmodules or use (replace `ci_res` with name of submodule):

		git config -f .gitmodules submodule.ci_res.branch stable
		
		
Configuration options (use `--global` to apply everywhere):

		git config status.submodulesummary 1
		git config submodule.recurse 1
